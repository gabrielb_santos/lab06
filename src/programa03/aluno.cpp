/**
* @file	 	aluno.cpp
* @brief	Arquivo de corpo contendo a implementação da classe Aluno
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	14/05/2017
* @date		20/05/2017S
*/ 

#include "aluno.h"

/** 
 * @brief	Função que retorna o nome do aluno
 * @return 	nome
 */
string Aluno::get_nome(){
	return nome;
}


/** 
 * @brief	Função que atribui um nome ao aluno
 * @param	v V nome
 */
void Aluno::set_nome(string v){
	nome =  v;
}


/** 
 * @brief	Função que retorna a nota do aluno
 * @return 	nota
 */
float Aluno::get_nota(){
	return nota;
}


/** 
 * @brief	Função que atribui uma nota ao aluno
 * @param	n N nota
 */
void Aluno::set_nota(float n){
	nota = n;
}


/** 
 * @brief	Função que retorna as faltas do aluno
 * @return 	faltas
 */
int Aluno::get_faltas(){
	return faltas;
}


/** 
 * @brief	Função que atribui um numero de faltas ao aluno
 * @param	f F faltas
 */
void Aluno::set_faltas(int f){
	faltas = f;
}


/** 
 * @brief	Função que retorna a matricula do aluno
 * @return 	matricula
 */
int Aluno::get_matricula(){
	return matricula;
}


/** 
 * @brief	Função que atribui um numero a matricula do aluno
 * @param	m M matricula
 */
void Aluno::set_matricula(int m){
	matricula =+ m;
}


/** 
 * @brief	construtor parametrizado
 * @param	i I nome
 * @param	n N nota
 * @param	f F faltas
 */
Aluno::Aluno(string i, float n, int f){
	total++;
	nome = i;
	nota = n;
	faltas = f;
	matricula = total;
	next = NULL;
	prev = NULL;
}


/** 
 * @brief	construtor parametrizado
 * @param	i I nome
 * @param	n N nota
 * @param	f F faltas
 * @param	m M matricula
 */
Aluno::Aluno(string i, float n, int f, int m){
	total++;
	nome = i;
	nota = n;
	faltas = f;
	matricula = m;
	next = NULL;
	prev = NULL;
}


/** 
 * @brief	construtor parametrizado para sentinelas
 * @param	i I nome
 */
Aluno::Aluno(string i){
	nome = i;
	nota = 0;
	faltas = 0;
	matricula = 0;
	next = NULL;
	prev = NULL;
}

/** 
 * @brief	construtor padrão
 */
Aluno::Aluno(){
	total++;
	nota = 0.0;
	nome = "Nome não cadastrado";
	faltas = 0;
	matricula = 0;
	next = NULL;
	prev = NULL;
}
