/**
* @file	 	menu.cpp
* @brief	Arquivo de corpo contendo a implementação das funções
*			que controlam o menu do programa
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	14/05/2017
* @date		20/05/2017
*/

#include "menu.h"


/** 
 * @brief	Função que realiza o cadastro de um aluno 
 *			em uma turma
 * @param	l L turma onde sera feito o cadastro
 */
void cadastro(Turma &l){

	cout << endl <<"Insira o nome completo do aluno: ";
	string nome;
	cin.ignore();
	getline(cin, nome, '\n');

	if(l.checa(nome) == false){

		cout << "Insira a nota do aluno: ";
		float nota;

		do{
			invalida(nota);
		}while(nota > 10.0 || nota < 0.0);

		cout << "Insira o numero de faltas do aluno: ";
		int faltas;
		invalida(faltas);

		l.insert_ordered(nome, nota, faltas);
		system("clear");

	}else{
		system("clear");
		cout << "\nAluno com mesmo nome já cadastrado\n";
	}

}


/** 
 * @brief	Função que pesquisa se um aluno ja foi cadastrado
 *			na turma
 * @param	l L turma onde sera feito a pesquisa
 */
void pesquisar(Turma &l){
	system("clear");
	string ache;
	cout << "Informe o nome do aluno: ";
	cin.ignore();
	getline(cin, ache, '\n');
	l.search(ache);
}


/** 
 * @brief	Função que limpa a tela
 */
void ClearScreen(){
    cout << "\033[2J\033[1;1H"; // codigo retirado da stackoverflow http://stackoverflow.com/questions/4062045/clearing-terminal-in-linux-with-c-code
}


/** 
 * @brief	Função que gerencia as funções no menu aluno
 * @param	l L turma onde será feita as operações
 * @param	turma_atual TURMA_ATUAL turma atual
 */
void acessar_turma(Turma &l, int turma_atual){

	int escolha;
	ClearScreen();

	inicio:
	cout << "\n---------Gerenciador de cadastros de alunos---------\n";
	cout << "\n1 - Cadastar novo aluno";
	cout << "\n2 - Pesquisar aluno na turma";
	cout << "\n3 - Média das notas da turma (";

	if(l.get_quant()==0){
		cout << "0)";
	 }else{
	 	cout << l.get_average() << ')';}

	cout << "\n4 - Quantidade de alunos da turma ("  << l.get_quant() << ')';
	cout << "\n5 - Lista de alunos";
	cout << "\n6 - Sair da turma atual\n\n";

	escolha = 0;
	do{	
		invalida(escolha);
		
	}while(escolha > 6 || escolha < 1);

	ClearScreen();
	switch(escolha){
		case 1:
			cadastro(l);
			goto inicio;
			break;

		case 2:
			pesquisar(l);
			goto inicio;
			break;

		case 3:
			goto inicio;
			break;
			

		case 4:
			goto inicio;
			break;
			
		case 6:
			cout << "\nSaindo da turma...\n";
			break;

		case 5:
			ClearScreen();
			l.print_turma(turma_atual);
			goto inicio;

		default:
			break;

	}
}


/** 
 * @brief	Função que o usuário escolhe a quantidade
 *			de turmas a serem cadastradas
 * @param	quant QUANT quantidade de turmas cadastradas
 */
int welcome(int &quant){
	
	INICIO:
	cout << "\n---------Bem-vindo ao Gerenciador de cadastro de turmas---------\n";
	cout << "\n1 - Carregar turmas anteriores";
	cout << "\n2 - Cadastar Novas turmas (isto apagará os dados salvos)\n\n";

	int escolha;

	do{
		invalida(escolha);
	}while(escolha != 1 && escolha!= 2);

	if(escolha == 2){
		cout << "\n\nQuantas turmas deseja cadastrar?\n\n(obs: só é possível cadastrar o número de turmas uma vez por sessão)\n\n";
		cout << "digite: ";
		invalida(quant);

		ClearScreen();

		if(quant == 1){
			cout << quant << " turma foi cadastrada\n";
		}else{
			cout << quant << " turmas foram cadastradas\n";
		}

		return 0;

	}else{
		std::ifstream getq("build/programa03/turmas.txt");
		getq >> quant;
		getq.close();

		if(quant == 0){
			ClearScreen();
			cout << "\nNão existe registro de turmas no sistema\n";
			goto INICIO;
		}else{
			if(quant == 1){
				cout << quant << " turma foi carregada\n";
			}else{
				cout << quant << " turmas foram carregadas\n";
			}
		}

		return escolha;
	}
	
}


/** 
 * @brief	Função muda o foco das operações para outra turma
 * @param	turma_atual TURMA_ATUAL turma atual
 * @param	quant QUANT quantidade de turmas cadastradas
 */
void troca_turma(int &turma_atual, int quant){
	INICIO:
	cout << "\nExiste(m) " << quant << " turma(s) cadastrada(s)";
	cout << "\n\nInforme o número da turma que deseja acessar: ";
	int escolha;
	do{
		invalida(escolha);

		if (escolha > quant || escolha < 1){
			ClearScreen();
			goto INICIO;
		}

	}while(escolha > quant || escolha < 1);
	ClearScreen();
	cout << "\nTurma " << escolha << " selecionada\n";
	turma_atual = escolha - 1;
}


/** 
 * @brief	Função que salva em um arquivo .txt todas as turmas
 *			cadastradas
 * @details	na primeira linha fica a quantidade de turmas na segunda
 *			a quantidade de alunos naquela turma e depois os dados 
 *			dos alunos.
 * @param	l L array de turmas
 * @param	quant QUANT quantidade de turmas cadastradas
 */
void salvar(Turma l[], int quant){
	std::ofstream backup;
	backup.open("build/programa03/turmas.txt", std::ios::out | std::ios::ate);
	backup << quant << endl;

	for (int i = 0; i < quant; ++i){ 
		backup << l[i].get_quant() << endl;
		for (int j = 0; j < l[i].get_quant(); ++j)
		{
			backup << l[i].search_nome(j) << endl;
			backup << l[i].search_nota(j) << endl;
			backup << l[i].search_faltas(j) << endl;
			backup << l[i].search_matricula(j) << endl;
		}
		backup << endl;
	}

	backup.close();
}


/** 
 * @brief	Função que teria '-' e substitui por espaços em branco
 * @param	nome NOME nome a ser convertido
 */
void conv_nome(string &nome){
	for (unsigned int i = 0; i < nome.size(); ++i){
		if (nome[i] == '-')
			{
				nome.erase(i, 1);
				nome.insert((i)," ");
			}
	}

}


/** 
 * @brief	Função que carrega dados de um arquivo .txt e estancia em um
 *			vetor de turmas
 * @details	devido a problemas lendo espaços em branco dos nomes, a string nome
 *			precisou ser convertida para ficar com '-' no lugar de ' '.
 * @param	l L array de turmas
 */
void carrega(Turma l[]){
	std::ifstream load;
	load.open("build/programa03/turmas.txt");

	string nome;
	float nota;
	int faltas;
	int matricula;
	int q;
	load >> q;
	int tam;
	for (int i = 0; i < q; ++i){ 
		load >> tam;
		for (int j = 0; j < tam; ++j){	
			load >> nome >> nota >> faltas >> matricula;
			conv_nome(nome);

			l[i].insert_ordered(nome, nota, faltas, matricula);
		}	
	}
	load.close();

}


/** 
 * @brief	Função que checa a validade da entrada fornecida pelo usuario
 * @param	num NUM numero a ser checado
 */
void invalida(int &num){
	while (!(cin >> num)) {
    cin.clear(); //clear bad input flag
    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //discard input
    cout << "Entrada inválida. Por favor, digite um valor válido.\n";
	}
}


/** 
* @brief	Função sobrecarregada para float que checa a validade da entrada fornecida pelo usuario
* @param	num NUM numero a ser checado
*/
void invalida(float &num){
	while (!(cin >> num)) {
    cin.clear(); 
    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    cout << "Entrada inválida. Por favor, digite um valor válido.\n";
	}
}
