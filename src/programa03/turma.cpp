/**
* @file	 	turma.cpp
* @brief	Arquivo de corpo contendo a implementação da classe Turma
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	14/05/2017
* @date		20/05/2017
*/

#include <iostream>
#include "turma.h"

using std::cout;
using std::endl;

int Aluno::total = 0; 

/** 
 * @brief	construtor padrão
 */
Turma::Turma(){
	quant = 0;
	head = new Aluno("TURMA: ");
	tail = new Aluno("FIM");

	head->next = tail;
	tail->prev = head;

	head->prev = NULL;
	tail->next = NULL;
}


/** 
 * @brief	destrutor padrão
 */
Turma::~Turma(){
	while(head){
		Aluno *n = head->next;
		delete head;
		head = n;
	}
}


/** 
 * @brief	Função que retorna a quantidade de
 *			elementos na lista
 * @return 	quant
 */
int Turma::get_quant(){
	return quant;
}


/** 
 * @brief	Função que atribui um valor quantidade de
 *			elementos da turma
 * @param	i I valor somado a total de elementos
 */
void Turma::set_quant(int i){
	quant += i;
}


/** 
 * @brief	Função que retorna a media aritimética
 *			das notas da turma
 * @return 	average
 */
float Turma::get_average(){
	Aluno *a = head->next;
	float av = 0.0;
	while(a != tail){
		av += a->get_nota();
		a = a->next;
	}
	average = av/quant;
	return average;
}


/** 
 * @brief	Função que insere um elemento no inicio da turma
 * @param	value VALUE nome atribuido a elemento
 * @param	x X nota
 * @param	f F faltas
 */
void Turma::insert_beggin(string value, float x, int f){
	Aluno *n = new Aluno(value, x, f);
	Aluno *h = head;

	h->next->prev = n;
	n->next = h->next;

	h->next = n;
	n->prev = h;

	set_quant(1);
}


/** 
 * @brief	Função que insere um elemento no final da turma
 * @param	value VALUE nome atribuido a elemento
 * @param	x X nota
 * @param	f F faltas
 */
void Turma::insert_end(string value, float x, int f){
	Aluno *n = new Aluno(value, x, f);
	Aluno *t = tail;

	t->prev->next = n;
	n->prev = t->prev;

	t->prev = n;
	n->next = t;

	set_quant(1);
}


/** 
 * @brief	Função que insere um elemento na turma apos um indice
 * @param	value VALUE nome atribuido a elemento
 * @param	index INDEX indice da lista
 * @param	x X nota
 * @param	f F faltas
 */
void Turma::insert_after(int index, string value, float x, int f){
	if(index > quant || index < 0){
		cout << "\nImpossívels adicionar nesta posição:\nPosição inexistente\n";
	}else{
		Aluno *insert;
		Aluno *n = new Aluno(value, x, f);
		int cont = 0;

		for (insert = head->next; cont < index; insert = insert->next)
		{
			cont++;
		}

		n->prev = insert->prev;
		insert->prev->next = n;
		n->next = insert;
		insert->prev = n;

		set_quant(1);
	}
}


/** 
 * @brief	Função que insere um aluno por ordem alfabética na turma
 * @param	value VALUE nome atribuido a elemento
 * @param	f F faltas
 */
void Turma::insert_ordered(string value, float n, int f){
	Aluno *ord = new Aluno(value, n, f);
	Aluno *print_Turma = head->next;
	while(print_Turma != tail && (print_Turma->get_nome() < ord->get_nome())){
		print_Turma = print_Turma->next;
	}

	ord->next = print_Turma;
	ord->prev = print_Turma->prev;
	print_Turma->prev->next =  ord;
	print_Turma->prev = ord;


	set_quant(1);

}


/** 
 * @brief	Função que insere um aluno por ordem alfabética na turma
 * @param	value VALUE nome atribuido a elemento
 * @param	f F faltas
 * @param	m M matricula
 */
void Turma::insert_ordered(string value, float n, int f, int m){
	Aluno *ord = new Aluno(value, n, f, m);
	Aluno *print_Turma = head->next;
	while(print_Turma != tail && (print_Turma->get_nome() < ord->get_nome())){
		print_Turma = print_Turma->next;
	}

	ord->next = print_Turma;
	ord->prev = print_Turma->prev;
	print_Turma->prev->next =  ord;
	print_Turma->prev = ord;


	set_quant(1);

}


/** 
 * @brief	Função que remove um elemento no final da turma
 */
void Turma::remove_end(){
	Aluno *remove = tail->prev;
	tail->prev = remove->prev;
	remove->prev->next = tail;
	delete remove;

	set_quant(-1);

}


/** 
 * @brief	Função que remove um elemento no inicio da turma
 */
void Turma::remove_beggin(){
	Aluno *remove = head->next;
	head->next = remove->next;
	remove->next->prev = head;
	delete remove;
	set_quant(-1);
}


/** 
 * @brief	Função que remove um elemento no final da turma
 * @param	index INDEX indice da lista
 */
void Turma::remove_index(int index){
	Aluno *remove = new Aluno();
	int cont = 0;
	for (remove = head->next; cont < index; remove = remove->next)
	{
		cont++;
	}

	remove->prev->next = remove->next;
	remove->next->prev = remove->prev;

	delete remove;
	set_quant(-1);
}


/** 
 * @brief	Função que procura por um aluno na turma
 * @param	value VALUE nome do aluno procurado
 */
void Turma::search(string value){
	int cont = 0;
	Aluno *search = head->next;
	for (; search; search = search->next)
	{
		if (search->get_nome() == value)
		{	
			cout << "\nAluno encontrado\n";
			cout << "Regristros:\n";
			cout << "\nNome: " << search->get_nome() << "\nNota: "<< search->get_nota();
			cout << "\nFaltas: " << search->get_faltas() << "\nMatricula: " << search->get_matricula();
			cont++;
		}
	}

	if(cont==0){
	 cout << "\nAluno não encontrado\n";
	}

}


/** 
 * @brief	Função que procura por um aluno na turma pelo seu nome
 * @param	m M indice a ser procurado
 */
string Turma::search_nome(int m){
	int cont = 0;
	Aluno *search = head->next;
	for (; cont < m; search = search->next){
		cont++;
	}
	string conv = search->get_nome();
	for (unsigned int i = 0; i < conv.length(); ++i)
	{
		if (conv[i] == ' ')
		{
			conv.erase(i, 1);
			conv.insert((i),"-");
		}
	}
	return conv;
}


/** 
 * @brief	Função que procura por um aluno na turma pela sua nota
 * @param	m M indice a ser procurado
 */
float Turma::search_nota(int m){
	Aluno *search = new Aluno();
	int cont = 0;
	for (search = head->next; cont < m; search = search->next)
	{
		cont++;
	}

	return search->get_nota();

}


/** 
 * @brief	Função que procura por um aluno na turma pela suas faltas
 * @param	m M indice a ser procurado
 */
int Turma::search_faltas(int m){
	int cont = 0;
	Aluno *search = head->next;
	for (; cont < m; search = search->next){
		cont++;
	}

	return search->get_faltas();
}


/** 
 * @brief	Função que procura por um aluno na turma pela sua matricula
 * @param	m M indice a ser procurado
 */
int Turma::search_matricula(int m){
	int cont = 0;
	Aluno *search = head->next;
	for (; cont < m; search = search->next){
		cont++;
	}

	return search->get_matricula();
}


/**
 * @brief 	Função que imprime a lista 
*/
void Turma::print_turma(int turma_atual){
	Aluno *current = head->next;
	cout << endl << head->get_nome() << turma_atual + 1 << endl;
	while(current != tail){
		cout << "\nNome: " << current->get_nome() << "\nNota: " << current->get_nota();
		cout << "\nFaltas: " << current->get_faltas();
		cout << "\nMatricula: " << current->get_matricula() << "\n\n";
		current = current->next;
	} cout << tail->get_nome() << endl;
}


/** 
 * @brief	Função que checa se existe um aluno com o mesmo nome na turma
 * @param	nome NOME nome do elemento procurado
 * @return 	retorna false caso não ache e retorna true caso ache
 */
bool Turma::checa(string nome){
	Aluno *search = head->next;
	for (; search; search = search->next)
	{
		if (search->get_nome() == nome)
		{	
			return true;
		}
	}

	return false;
}

