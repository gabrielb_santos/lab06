/**
* @file	 	main.cpp
* @brief	Programa que realiza cadastro e operações em turmas e alunos
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	14/05/2017
* @date		20/05/2017
*/


#include <iostream>
#include <string>
#include <fstream>
#include "turma.h"
#include "menu.h"

using std::endl; 
using std::cout;
using std::cin;
using std::string;

int main(){
	
	ClearScreen();
	int choise_turma;
	int quant = 0;

	int turma_atual;
	turma_atual = 0;  

	int x = welcome(quant);
	Turma *l = new Turma[quant];
	if(x == 1){
		carrega(l);
	}

	TELA1:
	ClearScreen();
	cout << "\n---------Gerenciador de cadastro de turmas---------\n";
	cout << "\n1 - Acessar turma atual (turma " << turma_atual + 1 << ")";
	cout << "\n2 - Acessar outra turma";
	cout << "\n3 - Quantidade de turmas cadastradas (" << quant << ')';
	cout << "\n4 - Salvar e sair\n\n";

	choise_turma = 0;

	do{	
		invalida(choise_turma);

	}while(choise_turma > 4 || choise_turma < 1);
	ClearScreen();

	switch(choise_turma){

		case 1:
			acessar_turma(l[turma_atual], turma_atual);
			goto TELA1;
			break;

		case 2:
			ClearScreen();
			troca_turma(turma_atual, quant);
			goto TELA1;
			break;

		case 3:
			goto TELA1;
			break;
			
		case 4:
			salvar(l, quant);
			
			
			cout << "\nSaindo...\n";
			goto FIM;

		default:
			break;

	}
	FIM:
	return 0;
	
}