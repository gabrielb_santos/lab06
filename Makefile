stack = ./bin/stack

list = ./bin/list

turma = ./bin/turma


INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc

CC = g++

RM = rm -rf

OBJS_Q01 = ./build/programa01/main.o ./build/programa01/stack.o ./build/programa01/tools.o 

OBJS_Q02 = ./build/programa02/main.o ./build/programa02/list.o

OBJS_Q03 = ./build/programa03/main.o ./build/programa03/turma.o ./build/programa03/menu.o ./build/programa03/aluno.o

CPPFLAGS = -Wall -pedantic -ansi -std=c++11

INC_01 = -I. -I$(INC_DIR)/programa01

INC_02 = -I. -I$(INC_DIR)/programa02

INC_03 = -I. -I$(INC_DIR)/programa03

.PHONY: init all programa01  programa02 programa03 doc clean 

all: programa01 programa02 programa03


debug: CFLAGS += -g -O0
debug: all

# Alvo (target) para a criação da estrutura de diretorios
# necessaria para a geracao dos arquivos objeto 	
init:	

	@mkdir -p $(DOC_DIR)
	@mkdir -p $(BIN_DIR)
	@mkdir -p $(OBJ_DIR)
	@mkdir -p $(OBJ_DIR)/programa01 $(OBJ_DIR)/programa02 $(OBJ_DIR)/programa03


programa01: $(stack) 

$(stack): $(OBJS_Q01)
	$(CC) $(OBJS_Q01) $(CPPFLAGS) $(INC_01) -o $@

$(OBJ_DIR)/programa01/main.o: $(SRC_DIR)/programa01/main.cpp
	$(CC) -c $(CPPFLAGS) $(INC_01) $^ -o $@

$(OBJ_DIR)/programa01/stack.o: $(SRC_DIR)/programa01/stack.cpp $(INC_DIR)/programa01/stack.h
	$(CC) -c $(CPPFLAGS) $(INC_01) $< -o $@

$(OBJ_DIR)/programa01/tools.o: $(SRC_DIR)/programa01/tools.cpp $(INC_DIR)/programa01/tools.h
	$(CC) -c $(CPPFLAGS) $(INC_01) $< -o $@



programa02: $(list)

$(list):	$(OBJS_Q02)
	$(CC) $^ $(CPPFLAGS) $(INC_02) -o $@

$(OBJ_DIR)/programa02/main.o: $(SRC_DIR)/programa02/main.cpp
	$(CC) -c $(CPPFLAGS) $(INC_02) $^ -o $@

$(OBJ_DIR)/programa02/list.o: $(SRC_DIR)/programa02/list.cpp $(INC_DIR)/programa02/list.h
	$(CC) -c $(CPPFLAGS) $(INC_02) $< -o $@



programa03: $(turma)

$(turma): $(OBJS_Q03)
	$(CC) $(OBJS_Q03) $(CPPFLAGS) $(INC_03) -o $@

$(OBJ_DIR)/programa03/main.o: $(SRC_DIR)/programa03/main.cpp
	$(CC) -c $(CPPFLAGS) $(INC_03) $^ -o $@

$(OBJ_DIR)/programa03/turma.o: $(SRC_DIR)/programa03/turma.cpp $(INC_DIR)/programa03/turma.h
	$(CC) -c $(CPPFLAGS) $(INC_03) $< -o $@

$(OBJ_DIR)/programa03/menu.o: $(SRC_DIR)/programa03/menu.cpp $(INC_DIR)/programa03/menu.h
	$(CC) -c $(CPPFLAGS) $(INC_03) $< -o $@	

$(OBJ_DIR)/programa03/aluno.o: $(SRC_DIR)/programa03/aluno.cpp $(INC_DIR)/programa03/aluno.h
	$(CC) -c $(CPPFLAGS) $(INC_03) $< -o $@	

doxy:
	doxygen Doxyfile

doc:
	$(RM) $(DOC_DIR)/*

clean:
	$(RM) $(OBJ_DIR)/programa01/* $(OBJ_DIR)/programa02/* $(OBJ_DIR)/programa03/*
	$(RM) $(BIN_DIR)/*
