/**
* @file	 	turma.h
* @brief	Arquivo de cabeçalho contendo a definição da classe Turma
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	14/05/2017
* @date		20/05/2017
*/

#ifndef TURMA_H
#define TURMA_H

#include <string>
#include "aluno.h"
using std::string;

class Turma{

	private:
		int quant;
		float average;

	public:


		/** 
		 * @brief	construtor padrão
		 */
		Turma();


		/** 
		 * @brief	destrutor padrão
		 */
		~Turma();

		Aluno *head;
		Aluno *tail;


		/** 
		 * @brief	Função que retorna a quantidade de
		 *			alunos na turma
		 * @return 	quant
		 */
		int get_quant();


		/** 
		 * @brief	Função que atribui um valor a quantidade de
		 *			alunos da turma
		 * @param	i I valor somado a total de elementos
		 */
		void set_quant(int i);


		/** 
		 * @brief	Função que retorna a media aritimética
		 *			das notas da turma
		 * @return 	average
		 */
		float get_average();


		/** 
		 * @brief	Função que insere um elemento no inicio da turma
		 * @param	value VALUE nome atribuido a elemento
		 * @param	x X nota
		 * @param	f F faltas
		 */
		void insert_beggin(string value, float x, int f);


		/** 
		 * @brief	Função que insere um elemento no final da turma
		 * @param	value VALUE nome atribuido a elemento
		 * @param	x X nota
		 * @param	f F faltas
		 */
		void insert_end(string value, float x, int f);


		/** 
		 * @brief	Função que insere um elemento na turma apos um indice
		 * @param	value VALUE nome atribuido a elemento
		 * @param	index INDEX indice da lista
		 * @param	x X nota
		 * @param	f F faltas
		 */
		void insert_after(int index, string value, float x, int f);


		/** 
		 * @brief	Função que insere um elemento por ordem alfabética na turma
		 * @param	value VALUE nome atribuido a elemento
		 * @param	f F faltas
		 */
		void insert_ordered(string value, float x, int f);


		/** 
		 * @brief	Função que insere um elemento por ordem alfabética na turma
		 * @param	value VALUE nome atribuido a elemento
		 * @param	f F faltas
		 * @param 	m M matricula
		 */
		void insert_ordered(string value, float x, int f, int m);


		/** 
		 * @brief	Função que remove um elemento no final da turma
		 */
		void remove_end();


		/** 
		 *	 @brief	Função que remove um elemento no inicio da turma
		 */
		void remove_beggin();


		/** 
		 * @brief	Função que remove um elemento no final da turma
		 * @param	index INDEX indice da lista
		 */
		void remove_index(int index);



		/** 
		 * @brief	Função que procura por um elemento na lista
		 * @param	value VALUE nome do elemento procurado
		 */
		void search(string value);


		/** 
		 * @brief	Função que procura por um aluno na turma pelo seu nome
		 * @param	m M indice a ser procurado
		 */
		string search_nome(int m);


		/** 
		 * @brief	Função que procura por um aluno na turma pela sua nota
		 * @param	m M indice a ser procurado
		 */
		float search_nota(int m);


		/** 
		 * @brief	Função que procura por um aluno na turma pela suas faltas
		 * @param	m M indice a ser procurado
		 */
		int search_faltas(int m);


		/** 
		 * @brief	Função que procura por um aluno na turma pela sua matricula
		 * @param	m M indice a ser procurado
		 */
		int search_matricula(int m);


		/**
		 * @brief 	Função que imprime a lista 
		*/
		void print_turma(int turma_atual);


		/** 
		 * @brief	Função que checa se existe um aluno com o mesmo nome na turma
		 * @param	nome NOME nome do elemento procurado
		 * @return 	retorna false caso não ache e retorna true caso ache
		 */
		bool checa(string nome);


};

#endif