/**
* @file	 	menu.h
* @brief	Arquivo de cabeçalho contendo a definição das funções
*			que controlam o menu do programa
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	14/05/2017
* @date		20/05/2017
*/


#ifndef MENU_H
#define MENU_H

#include <iostream>
#include <string>
#include <fstream>
#include <limits>
#include "turma.h"

using std::endl;
using std::cout;
using std::cin;
using std::string;


/** 
 * @brief	Função que realiza o cadastro de um aluno 
 *			em uma turma
 * @param	l L turma onde sera feito o cadastro
 */
void cadastro(Turma &l);
	

/** 
 * @brief	Função que pesquisa se um aluno ja foi cadastrado
 *			na turma
 * @param	l L turma onde sera feito a pesquisa
 */
void pesquisar(Turma &l);


/** 
 * @brief	Função que limpa a tela
 */
void ClearScreen();


/** 
 * @brief	Função que gerencia as funções no menu aluno
 * @param	l L turma onde será feita as operações
 * @param	turma_atual TURMA_ATUAL turma atual
 */
void acessar_turma(Turma &l, int turma_atual);


/** 
 * @brief	Função que o usuário escolhe a quantidade
 *			de turmas a serem cadastradas
 * @param	quant QUANT quantidade de turmas cadastradas
 */
void troca_turma(int &turma_atual, int quant);


/** 
 * @brief	Função muda o foco das operações para outra turma
 * @param	turma_atual TURMA_ATUAL turma atual
 * @param	quant QUANT quantidade de turmas cadastradas
 */
int welcome(int &quant);
	

/** 
 * @brief	Função que salva em um arquivo .txt todas as turmas
 *			cadastradas
 * @details	na primeira linha fica a quantidade de turmas na segunda
 *			a quantidade de alunos naquela turma e depois os dados 
 *			dos alunos.
 * @param	l L array de turmas
 * @param	quant QUANT quantidade de turmas cadastradas
 */
void salvar(Turma l[], int quant);


/** 
 * @brief	Função que teria '-' e substitui por espaços em branco
 * @param	nome NOME nome a ser convertido
 */
void conv_nome(string &nome);


/** 
 * @brief	Função que carrega dados de um arquivo .txt e estancia em um
 *			vetor de turmas
 * @details	devido a problemas lendo espaços em branco dos nomes, a string nome
 *			precisou ser convertida para ficar com '-' no lugar de ' '.
 * @param	l L array de turmas
 */
void carrega(Turma l[]);


/** 
 * @brief	Função que checa a validade da entrada fornecida pelo usuario
 * @param	num NUM numero a ser checado
 */
void invalida(int &num); //parte da função retirada do link: http://stackoverflow.com/questions/4798936/numeric-limits-was-not-declared-in-this-scope-no-matching-function-for-call-t


/** 
* @brief	Função sobrecarregada para float que checa a validade da entrada fornecida pelo usuario
* @param	num NUM numero a ser checado
*/
void invalida(float &num); //parte da função retirada do link: http://stackoverflow.com/questions/4798936/numeric-limits-was-not-declared-in-this-scope-no-matching-function-for-call-t


#endif