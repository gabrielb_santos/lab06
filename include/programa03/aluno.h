/**
* @file	 	aluno.h
* @brief	Arquivo de cabeçalho contendo a definição da classe Aluno
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	14/05/2017
* @date		20/05/2017S
*/


#ifndef ALUNO_H
#define ALUNO_H

#include <string>

using std::string;


class Aluno{
	private:
		string nome;
		float nota;
		int faltas;
		int matricula;

	public:
		static int total;
		Aluno *next;
		Aluno *prev;

		/** 
		* @brief	Função que retorna o nome do aluno
		* @return 	nome
		*/
		string get_nome();


		/** 
		 * @brief	Função que atribui um nome ao aluno
		 * @param	v V nome
		 */
		void set_nome(string v);


		/** 
		 * @brief	Função que retorna a nota do aluno
		 * @return 	nota
		 */
		float get_nota();


		/** 
		 * @brief	Função que atribui uma nota ao aluno
		 * @param	n N nota
		 */
		void set_nota(float n);


		/** 
		 * @brief	Função que retorna as faltas do aluno
		 * @return 	faltas
		 */
		int get_faltas();


		/** 
		 * @brief	Função que atribui um numero de faltas ao aluno
		 * @param	f F faltas
		 */
		void set_faltas(int f);


		/** 
		 * @brief	Função que retorna a matricula do aluno
		 * @return 	matricula
		 */
		int get_matricula();


		/** 
		 * @brief	Função que atribui um numero a matricula do aluno
		 * @param	m M matricula
		 */
		void set_matricula(int m);


		/** 
		 * @brief	construtor parametrizado
		 * @param	i I nome
		 * @param	n N nota
		 * @param	f F faltas
		 */
		Aluno(string i, float n, int f);


		/** 
		 * @brief	construtor parametrizado
		 * @param	i I nome
		 * @param	n N nota
		 * @param	f F faltas
		 * @param	m M matricula
		 */
		Aluno(string i, float n, int f, int m);


		/** 
		 * @brief	construtor parametrizado para sentinelas
		 * @param	i I nome
		 */
		Aluno(string n);

		/** 
		 * @brief	construtor padrão
		 */
		Aluno();


		/** 
		 * @brief	destrutor padrão
		 */
		~Aluno(){};
		};



#endif